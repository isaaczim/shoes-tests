﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Shoes.Tests
{
    class Program
    {
        //Create reference for browser
        IWebDriver driver = new ChromeDriver(@"C:\_repos\Shoes.Tests\packages\chromedriver_win32");


        //This method is for my own testing/debugging purposes
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver(@"C:\_repos\Shoes.Tests\packages\chromedriver_win32");

            driver.Navigate().GoToUrl("http://shoestore-manheim.rhcloud.com/");
            IWebElement element = driver.FindElement(By.LinkText("November"));

            //Preform Operation
            element.Click();

            IWebElement shoeList = driver.FindElement(By.Id("shoe_list"));
            IWebElement shoe = shoeList.FindElement(By.ClassName("shoe_result"));
            IWebElement price = shoe.FindElement(By.ClassName("shoe_price"));
            IWebElement description = shoe.FindElement(By.ClassName("shoe_description"));
            IWebElement image = shoe.FindElement(By.TagName("img"));
            IWebElement emailElement = driver.FindElement(By.Id("remind_email_form"));
            emailElement.FindElement(By.TagName("input")).SendKeys(";alksdfj");

            emailElement.Submit();
        }

        [SetUp]
        public void Initialize()
        {
            //Navigate to Shoes Page
            driver.Navigate().GoToUrl("http://shoestore-manheim.rhcloud.com/");
        }

        [TestCase("January")]
        [TestCase("February")]
        [TestCase("March")]
        [TestCase("April")]
        [TestCase("May")]
        [TestCase("June")]
        [TestCase("July")]
        [TestCase("August")]
        [TestCase("September")]
        [TestCase("October")]
        [TestCase("November")]
        [TestCase("December")]
        public void TestPrice(string month)
        {
            //Find Element
            IWebElement element = driver.FindElement(By.LinkText(month));

            //Preform Operation
            element.Click();

            //Find Price
            List<IWebElement> prices = new List<IWebElement>();
            ReadOnlyCollection<IWebElement> shoes = driver.FindElements(By.ClassName("shoe_result"));
            foreach (var webElement in shoes)
            {
                prices.Add(webElement.FindElement(By.ClassName("shoe_price")));
            }

            int i = 0;
            foreach (var price in prices)
            {
                if (price.Displayed)
                {
                    i++;
                }
            }

            Console.WriteLine(i + ", " + prices.Count);

            //Month should display a small Blurb of each shoe
            Assert.AreEqual(prices.Count, i);

        }

        [TestCase("January")]
        [TestCase("February")]
        [TestCase("March")]
        [TestCase("April")]
        [TestCase("May")]
        [TestCase("June")]
        [TestCase("July")]
        [TestCase("August")]
        [TestCase("September")]
        [TestCase("October")]
        [TestCase("November")]
        [TestCase("December")]
        public void TestPriceDecemberFails(string month)
        {
            //Find Element
            IWebElement element = driver.FindElement(By.LinkText(month));

            //Preform Operation
            element.Click();

            //Find Price
            IWebElement shoeList = driver.FindElement(By.Id("shoe_list"));
            IWebElement shoe = shoeList.FindElement(By.ClassName("shoe_result"));
            IWebElement price = shoe.FindElement(By.ClassName("shoe_price"));

            //Month should display a small Blurb of each shoe
            Assert.IsNotNull(price.Text);
        }


        [TestCase("January")]
        [TestCase("February")]
        [TestCase("March")]
        [TestCase("April")]
        [TestCase("May")]
        [TestCase("June")]
        [TestCase("July")]
        [TestCase("August")]
        [TestCase("September")]
        [TestCase("October")]
        [TestCase("November")]
        [TestCase("December")]
        public void TestDescription(string month)
        {
            //Find Element
            IWebElement element = driver.FindElement(By.LinkText(month));

            //Preform Operation
            element.Click();

            //Find Description
            List<IWebElement> descriptions = new List<IWebElement>();
            ReadOnlyCollection<IWebElement> shoes = driver.FindElements(By.ClassName("shoe_result"));
            foreach (var webElement in shoes)
            {
                descriptions.Add(webElement.FindElement(By.ClassName("shoe_description")));
            }

            int i = 0;
            foreach (var description in descriptions)
            {
                if (description.Displayed)
                {
                    i++;
                }
            }

            Console.WriteLine(i +", " + descriptions.Count);
            
            //Month should display an image each shoe being released
            Assert.AreEqual(descriptions.Count, i);
        }

        [TestCase("January")]
        [TestCase("February")]
        [TestCase("March")]
        [TestCase("April")]
        [TestCase("May")]
        [TestCase("June")]
        [TestCase("July")]
        [TestCase("August")]
        [TestCase("September")]
        [TestCase("October")]
        [TestCase("November")]
        [TestCase("December")]
        public void TestDescriptionDecemberFails(string month)
        {
            //Find Element
            IWebElement element = driver.FindElement(By.LinkText(month));

            //Preform Operation
            element.Click();

            //Find Description
            IWebElement shoeList = driver.FindElement(By.Id("shoe_list"));
            IWebElement shoe = shoeList.FindElement(By.ClassName("shoe_description"));


            //Month should display an image each shoe being released
            Assert.IsNotEmpty(shoe.Text);
        }


        [TestCase("January")]
        [TestCase("February")]
        [TestCase("March")]
        [TestCase("April")]
        [TestCase("May")]
        [TestCase("June")]
        [TestCase("July")]
        [TestCase("August")]
        [TestCase("September")]
        [TestCase("October")]
        [TestCase("November")]
        [TestCase("December")]
        public void TestImage(string month)
        {
            //Find Element
            IWebElement element = driver.FindElement(By.LinkText(month));

            //Preform Operation
            element.Click();

            //Find Image
            IWebElement shoeList = driver.FindElement(By.Id("shoe_list"));
            IWebElement shoe = shoeList.FindElement(By.ClassName("shoe_result"));
            IWebElement image = shoe.FindElement(By.TagName("img"));
            
            //Each shoe should have a suggested price pricing
            Assert.IsTrue(image.Displayed);
        }

        [TestCase("test@test.com")]
        public void TestEmailSuccess(string email)
        {
            IWebElement emailElement = driver.FindElement(By.Id("remind_email_form"));

            emailElement.FindElement(By.TagName("input")).SendKeys(email);

            emailElement.Submit();

            IWebElement notice = driver.FindElement(By.ClassName("flash"));

            Assert.IsTrue(notice.Displayed);
        }

        [TestCase("Test")]
        public void TestEmailFail(string email)
        {
            IWebElement emailElement = driver.FindElement(By.Id("remind_email_form"));

            emailElement.FindElement(By.TagName("input")).SendKeys(email);

            emailElement.Submit();

            IWebElement notice = driver.FindElement(By.ClassName("flash"));

            Assert.IsFalse(notice.Displayed);
        }



        //[TearDown]
        //public void CleanUp()
        //{
        //    driver.Close();
        //}
    }
}
